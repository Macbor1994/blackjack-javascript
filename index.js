///////////////////////////OBJECT WITH PLAYERS DATA/////////////////////////////
var player = {name: "Maciej", chips: 150}
var nameEl = document.getElementById("playerName");
var chipsEl = document.getElementById("playerChips");

nameEl.textContent += player.name;
chipsEl.textContent += player.chips;
//////////////////////////RANDOM CARDS FUNCTION/////////////////////////////////
function getRandomCard(){
  var random = Math.floor(Math.random()*13) + 1;
  if (random > 9)
    {random = 10}
  if (random === 1)
    {random = 11}
  return random; }
////////////////////////////////////////////////////////////////////////////////
var cardsEl = document.getElementById("cards-El");
var firstCard = 0;
var secondCard = 0;
var newCardEl = 0;
var allCards = [firstCard, secondCard];

var sumEl = document.querySelector("#sum-El");
var sum = firstCard + secondCard;

var messageEl = document.getElementById("message-El");
var message = "";
var messageContinue = "Your result is less than 21, do you want to take another card ?";
var messageWon = "Your result is equal to 21, you won";
var messageLose = "Your result is higher than 21, you lost";

var wonlose = false;
var isAlive = true;
////////////////////////////START GAME FUNCTION/////////////////////////////////
function startGame() {
  firstCard = getRandomCard();
  secondCard = getRandomCard();

  sum = firstCard + secondCard;
  allCards = [firstCard, secondCard];

  if (sum < 21) {
    message = messageContinue;
    document.querySelector("#start-btn").style.visibility = 'hidden';
    document.querySelector("#card-btn").style.visibility = 'visible';
  }
  if (sum === 21) {
    message = messageWon;
    wonlose = true;
    document.querySelector("#start-btn").style.visibility = 'hidden';
    document.querySelector("#again-btn").style.visibility = 'visible';
  }
  if (sum > 21) {
    message = messageLose;
    isAlive = false;
    document.querySelector("#start-btn").style.visibility = 'hidden';
    document.querySelector("#again-btn").style.visibility = 'visible';
  }

  messageEl.textContent = message;

  for(let i=0;i<allCards.length;){
    cardsEl.textContent += allCards[i];
    i++
    if (i < allCards.length){
      cardsEl.textContent += " and ";
    }
  }

  sumEl.textContent = "Sum: " + sum;
}
//////////////////////////////NEW CARD FUNCTION/////////////////////////////////
function newCard(){
  newCardEl = getRandomCard();

  allCards.push(newCardEl);
  for (let i=2; i < allCards.length ;i++){
    cardsEl.textContent +=" and " + allCards[i];
    allCards.pop();
  }

  sum += newCardEl;
  sumEl.textContent = "Sum: " + sum;

  if (sum === 21) {
    message = messageWon;
    wonlose = true;
    messageEl.textContent = message;

    document.querySelector("#card-btn").style.visibility = 'hidden';
    document.querySelector("#again-btn").style.visibility = 'visible';}

  if (sum > 21) {
    message = messageLose;
    isAlive = false;
    messageEl.textContent = message;

    document.querySelector("#card-btn").style.visibility = 'hidden';
    document.querySelector("#again-btn").style.visibility = 'visible';
  }
}
///////////////////////////PLAY AGAIN FUNCTION//////////////////////////////////
function playAgain(){
  allCards = [];
  cardsEl.textContent = "Cards: ";
  sumEl.textContent = "Sum:";

  document.querySelector("#start-btn").style.visibility = 'visible';
  document.querySelector("#again-btn").style.visibility = 'hidden';

  messageEl.textContent = "Want to play a round?";

  wonlose = false;
  isAlive = true;
}
////////////////////////////NIGHTMODEFUNCTION///////////////////////////////////
var backgroundNormal = true;

function turnNight() {
  if (backgroundNormal == true)
    {
    document.body.style.backgroundColor = "#013003";
    backgroundNormal = false;
    return;
    }
  if (backgroundNormal == false)
    {
    document.body.style.backgroundColor = "darkgreen";
    backgroundNormal = true;
    return;
    }
}
